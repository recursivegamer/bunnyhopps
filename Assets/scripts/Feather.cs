﻿using UnityEngine;

public class Feather : MonoBehaviour
{
    private GameObject featherSpawn;
    public string featherTag = "featherSpawn";

    private GameObject MouseSpawn;
    public string MouseSpawnTag = "mouseSpawn";
    public float points = 100;
    private GameObject progressBar;
    public string progressTag = "progressBar";

    // Use this for initialization
    private void Start()
    {
    }

    /*---------------------------------------------------
     * finds the different spawn objects then determines
     * if this object has collided with the player object
     * */
    private void OnCollisionEnter(Collision col)
    {
        if (MouseSpawn == null)
            MouseSpawn = GameObject.FindGameObjectWithTag(MouseSpawnTag);
        if (progressBar == null)
            progressBar = GameObject.FindGameObjectWithTag(progressTag);
        if (featherSpawn == null)
            featherSpawn = GameObject.FindGameObjectWithTag(featherTag);
        if (col.gameObject.tag == "Player")
        {
            progressBar.GetComponent<Progress>().hitFeather();
            progressBar.GetComponent<Progress>().increaseScore(points);
            featherSpawn.GetComponent<FeatherSpawn>().removeFeather();
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    private void Update()
    {
    }
}