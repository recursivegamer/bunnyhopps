﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    private static readonly float buttonWidth = 10;
    private static readonly float buttonHeight = 15;
    public Texture helpButton;

    private Vector2 helpButtonPos = new Vector2(Screen.width / 2 - Screen.width / (buttonWidth * 2),
        Screen.height / 2 + Screen.height / (buttonHeight * 2));

    private Vector2 helpButtonSize = new Vector2(Screen.width / buttonWidth, Screen.height / buttonHeight);

    public GUIStyle myStyle;

    public Texture playButton;

    private Vector2 playButtonPos = new Vector2(Screen.width / 2 - Screen.width / (buttonWidth * 2),
        Screen.height / 2 - Screen.height / (buttonHeight * 2));

    private Vector2 playButtonSize = new Vector2(Screen.width / buttonWidth, Screen.height / buttonHeight);
    private bool showHelp;
    public Texture tutorial;

    // Use this for initialization
    private void Start()
    {
    }

    /*-------------------------------------------------
     * displays the menu buttons and handles any button
     * presses.
     * */
    private void OnGUI()
    {
        if (!showHelp)
        {
            GUI.DrawTexture(new Rect(playButtonPos.x, playButtonPos.y, playButtonSize.x, playButtonSize.y), playButton);
            if (GUI.Button(new Rect(playButtonPos.x, playButtonPos.y, playButtonSize.x, playButtonSize.y), ""))
                SceneManager.LoadScene("BunnyTest");
            GUI.DrawTexture(new Rect(helpButtonPos.x, helpButtonPos.y, helpButtonSize.x, helpButtonSize.y), helpButton);
            if (GUI.Button(new Rect(helpButtonPos.x, helpButtonPos.y, helpButtonSize.x, helpButtonSize.y), ""))
                showHelp = true;
        }
        if (showHelp)
            if (GUI.Button(new Rect(0, 0, Screen.width, Screen.height), tutorial))
                showHelp = false;
    }

    // Update is called once per frame
    private void Update()
    {
    }
}