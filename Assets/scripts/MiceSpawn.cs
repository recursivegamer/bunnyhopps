﻿using UnityEngine;

public class MiceSpawn : MonoBehaviour
{
    public GameObject mouseObject;
    public float numOfMice;
    private bool paused;

    public float totalNumOfMice = 10f;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        if (!paused)
            spawnMice();
    }


    /*----------------------------------------------------
     * spawns the mouse object randomly throughout the 
     * stage outside of the players view
     * */
    private void spawnMice()
    {
        if (numOfMice <= totalNumOfMice)
        {
            var bunny = GameObject.Find("Bunny");
            var bunnyPosition = bunny.transform.position.x;
            var spawnLeft = getSideToSpawn(bunnyPosition);
            Vector3 position;
            if (spawnLeft)
                position = new Vector3(Random.Range(-45f, bunnyPosition - 10), .75f, 0);
            else
                position = new Vector3(Random.Range(bunnyPosition + 10, 45f), .75f, 0);
            Instantiate(mouseObject, position, Quaternion.identity);
            numOfMice++;
        }
    }

    // Determines if there is more room on the left or right side of the player to spawn mice
    private bool getSideToSpawn(float bunnyPosition)
    {
        if (bunnyPosition >= 0)
            return true;
        return false;
    }

    public void removeMouse()
    {
        numOfMice--;
    }

    private void OnPauseGame()
    {
        paused = true;
    }

    private void OnResumeGame()
    {
        paused = false;
    }

    public void increaseSpawn(float miceIncrease)
    {
        totalNumOfMice += miceIncrease;
    }
}