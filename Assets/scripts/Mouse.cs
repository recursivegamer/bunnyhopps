﻿using UnityEngine;

public class Mouse : MonoBehaviour
{
    public float deadTime = 5;
    private float directionTimer;
    public GameObject mouse;
    public Vector3 mouseMovementSpeed;
    private GameObject MouseSpawn;
    public string MouseSpawnTag = "mouseSpawn";
    private bool paused;
    public float points = 200;
    private GameObject progressBar;
    public string progressTag = "progressBar";
    private float startTime;

    // Use this for initialization
    private void Start()
    {
        startTime = Time.time;
        directionTimer = Random.Range(1, 5);
    }

    // Update is called once per frame
    private void Update()
    {
        if (!paused)
        {
            mouseMovement();
            animateMouse();
        }
    }


    /*---------------------------------------------------
     * randomly selects a direction for the mouse to move
     * then sets the velocity of the mouse in that direction
     * */
    private void mouseMovement()
    {
        var direction = Random.Range(0, 2);
        var currentTime = Time.time;
        if (currentTime - startTime > directionTimer)
        {
            if (direction == 0)
                mouseMovementSpeed = -mouseMovementSpeed;
            else
                mouseMovementSpeed = -mouseMovementSpeed;
            directionTimer = Random.Range(1, 5);
            startTime = currentTime;
        }
        GetComponent<Rigidbody>().velocity = mouseMovementSpeed;
    }


    /*------------------------------------------------------
     * checks any collisions only collisions with the player
     * and collsions from the top will destroy the mouse
     * */
    private void OnCollisionEnter(Collision col)
    {
        var cont = col.contacts[0];
        var contact = cont.normal;
        if (MouseSpawn == null)
            MouseSpawn = GameObject.FindGameObjectWithTag(MouseSpawnTag);
        if (progressBar == null)
            progressBar = GameObject.FindGameObjectWithTag(progressTag);
        if (Vector3.Dot(contact, Vector3.down) > .5f && col.gameObject.tag == "Player")
        {
            MouseSpawn.GetComponent<MiceSpawn>().removeMouse();
            progressBar.GetComponent<Progress>().hitMouse();
            progressBar.GetComponent<Progress>().increaseScore(points);
            mouse.transform.localScale = new Vector3(mouse.transform.localScale.x, .25f, mouse.transform.localScale.z);
            var pos = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - .25f,
                gameObject.transform.position.z);
            var mouseDead = Instantiate(mouse, pos, Quaternion.identity);
            Destroy(mouseDead, deadTime);
            Destroy(gameObject);
        }
    }

    private void OnPauseGame()
    {
        paused = true;
    }

    private void OnResumeGame()
    {
        paused = false;
    }


    /* animates mouse based on velocity of the object*/
    private void animateMouse()
    {
        if (GetComponent<Rigidbody>().velocity.x > 0)
            GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(0f, .5f));
        else
            GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(0f, 0f));
    }
}