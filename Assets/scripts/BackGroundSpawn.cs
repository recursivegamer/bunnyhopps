﻿using UnityEngine;

public class BackGroundSpawn : MonoBehaviour
{
    public GameObject[] backgroundObjects = new GameObject[5];

    // Use this for initialization
    private void Start()
    {
        spawnBackground();
    }


    /*-----------------------------------------------------------
     * Spawn the vegitation semi randomly throughout the level
     * */
    private void spawnBackground()
    {
        for (int i = 0; i < 50; i += 5)
        {
            int randomNum = (int) Random.Range(0f, 5f);
            Vector3 position = new Vector3(Random.Range(i - 2.5f, i + 2.5f),
                backgroundObjects[randomNum].transform.lossyScale.y / 2 + .5f, 31f);
            Instantiate(backgroundObjects[randomNum], position, Quaternion.identity);
            randomNum = (int) Random.Range(0f, 5f);
            Vector3 position2 = new Vector3(Random.Range(-i - 2.5f, -i + 2.5f),
                backgroundObjects[randomNum].transform.lossyScale.y / 2 + .5f, 31f);
            Instantiate(backgroundObjects[randomNum], position2, Quaternion.identity);
        }
    }

    // Update is called once per frame
    private void Update()
    {
    }
}