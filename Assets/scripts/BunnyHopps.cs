﻿using UnityEngine;

public class BunnyHopps : MonoBehaviour
{
    public Vector3 arielMovement;
    public Vector3 bouncyness;
    public bool directionleft;
    private bool grounded = true;
    private bool paused;
    public float speedLimitMax = 15f;

    // Use this for initialization
    private void Start()
    {
    }

    /*----------------------------------------------
     * calculates the position of the mouse in 
     * relation to the player and moving the player
     * in the intended direction.
     * */
    private void jump()
    {
        if (grounded)
        {
            if (Camera.main.ScreenPointToRay(Input.mousePosition).origin.x > transform.position.x)
            {
                //jump right
                getRigidBody().AddForce(bouncyness);
                directionleft = false;
            }
            else
            {
                //jump left
                var jumpBack = bouncyness;
                jumpBack.x = -bouncyness.x;
                getRigidBody().AddForce(jumpBack);
                directionleft = true;
            }
            grounded = false;
        }
        else
        {
            if (Camera.main.ScreenPointToRay(Input.mousePosition).origin.x > transform.position.x)
            {
                //jump right
                getRigidBody().AddForce(arielMovement);
            }
            else
            {
                //jump left
                var jumpBack = arielMovement;
                jumpBack.x = -arielMovement.x;
                getRigidBody().AddForce(jumpBack);
            }
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        var cont = col.contacts[0];
        var contact = cont.normal;
        // if the player hits something beneith them allow jumping
        if (Vector3.Dot(contact, Vector3.up) > .5f)
            grounded = true;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && !paused)
            jump();
        if (!paused)
            animateBunny();
        speedLimit();
    }

    private void OnPauseGame()
    {
        paused = true;
    }

    private void OnResumeGame()
    {
        paused = false;
    }


    /*------------------------------------------------
     * changes direction and position player displays
     * */
    private void animateBunny()
    {
        if (!grounded)
        {
            if (getRigidBody().velocity.x < 0)
                getRenderer().material.SetTextureOffset("_MainTex", new Vector2(0f, -.01f));
            else
                getRenderer().material.SetTextureOffset("_MainTex", new Vector2(0f, .24f));
        }
        else
        {
            if (getRigidBody().velocity.x < 0)
                getRenderer().material.SetTextureOffset("_MainTex", new Vector2(0f, .49f));
            else
                getRenderer().material.SetTextureOffset("_MainTex", new Vector2(0f, .74f));
        }
    }

    private Rigidbody getRigidBody()
    {
        return GetComponent<Rigidbody>();
    }

    private Renderer getRenderer()
    {
        return GetComponent<Renderer>();
    }

    /*---------------------------------------------------
     * limmits the maximum speed of the players movement
     * */
    private void speedLimit()
    {
        if (GetComponent<Rigidbody>().velocity.magnitude > speedLimitMax)
        {
            var limit = GetComponent<Rigidbody>().velocity.normalized;
            limit = limit * speedLimitMax;
            GetComponent<Rigidbody>().velocity = limit;
        }
    }
}