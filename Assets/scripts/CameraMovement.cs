﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject player;

    // Use this for initialization
    private void Start()
    {
    }

    // Update is called once per frame
    private void Update()
    {
        moveCamera();
    }

    /*-------------------------------------------------
     * moves the camera to follow the player character
     * */
    private void moveCamera()
    {
        transform.position = new Vector3(player.transform.position.x, transform.position.y, transform.position.z);
    }
}