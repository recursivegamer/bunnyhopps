﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Progress : MonoBehaviour
{
    public Font bunnyFont;
    public float featherHitProgress = .1f;
    private GameObject featherSpawn;
    public string featherTag = "featherSpawn";
    private bool gameOver;
    public float mouseHitProgress = .05f;

    private GameObject MouseSpawn;
    public string MouseSpawnTag = "mouseSpawn";
    private bool paused;
    private Vector2 pos = new Vector2(50, 60);


    private float progress;
    public Texture2D progressBarEmpty;
    public Texture2D progressBarFull;
    public float progressSpeed = .01f;
    private float score;
    private Vector2 scorePos = new Vector2(Screen.width - Screen.width / 5, 40);
    private Vector2 scoreSize = new Vector2(Screen.width, Screen.height);
    private Vector2 size = new Vector2(Screen.width / 15, Screen.height / 20);
    public float spawnRateChange = 10;
    private float startTime;
    public float totalFeathersToRemove = 1;
    public float totalMiceToAdd = 3;
    private Vector2 totalScorePos = new Vector2(Screen.width / 2 - 50, 60);

    private Vector2 totalScoreSize = new Vector2(100, 40);
    public Texture tryAgain;
    private Vector2 tryAgainPos = new Vector2(Screen.width / 2 - 75, 145);
    private Vector2 tryAgainSize = new Vector2(150, 100);
    public Texture tutorial;

    // Use this for initialization
    private void Start()
    {
        startTime = Time.time;
    }

    /*-------------------------------------------------
     * Displays:
     *  firstTimePlayInformation
     *  ProgressBar
     *  Score
     *  TryAgainButton
     *  HighScore
     * */
    private void OnGUI()
    {
        var bunnyStyle = new GUIStyle();
        bunnyStyle.fontSize = 30;
        bunnyStyle.font = bunnyFont;
        GUI.DrawTexture(new Rect(pos.x, pos.y, size.x + 2f, size.y), progressBarEmpty);
        GUI.DrawTexture(new Rect(pos.x + 1f, pos.y + 1f, size.x * Mathf.Clamp01(progress), size.y - 2f),
            progressBarFull);
        GUI.Label(new Rect(pos.x - 40f, pos.y - 50f, size.x, size.y), "Fairy Arrival", bunnyStyle);
        GUI.Label(new Rect(scorePos.x, scorePos.y, scoreSize.x, scoreSize.y), "Score\n" + score, bunnyStyle);
        if (gameOver)
        {
            if (!PlayerPrefs.HasKey("HighScore"))
            {
                PlayerPrefs.SetInt("HighScore", (int) score);
            }
            else
            {
                if (score >= PlayerPrefs.GetInt("HighScore"))
                    PlayerPrefs.SetInt("HighScore", (int) score);
            }
            GUI.Label(new Rect(totalScorePos.x, totalScorePos.y, totalScoreSize.x, totalScoreSize.y),
                "High Score\n" + PlayerPrefs.GetInt("HighScore"), bunnyStyle);
            if (GUI.Button(new Rect(tryAgainPos.x, tryAgainPos.y, tryAgainSize.x, tryAgainSize.y), tryAgain))
                SceneManager.LoadScene("BunnyTest");
        }
        if (!PlayerPrefs.HasKey("FirstPlay"))
        {
            var objects = FindObjectsOfType(typeof(GameObject));
            foreach (GameObject go in objects)
                go.SendMessage("OnPauseGame", SendMessageOptions.DontRequireReceiver);
            if (GUI.Button(new Rect(0, 0, Screen.width, Screen.height), tutorial))
            {
                foreach (GameObject go in objects)
                    go.SendMessage("OnResumeGame", SendMessageOptions.DontRequireReceiver);
                PlayerPrefs.SetFloat("FirstPlay", 1);
            }
        }
    }


    // Update is called once per frame
    private void FixedUpdate()
    {
        if (!paused)
        {
            progress += progressSpeed;
            checkForGameOver();
            changeSpawns();
            tryAgainPos = new Vector2(Screen.width / 2 - 75, 145);
            totalScorePos = new Vector2(Screen.width / 2 - 50, 60);
        }
    }

    //increments the progress bar based on a mouse collision
    public void hitMouse()
    {
        progress += mouseHitProgress;
    }

    //decrements the progress bar based on a feather collision
    public void hitFeather()
    {
        if (progress > 0)
            progress -= featherHitProgress;
    }

    //increases score based on collision with feathers and mice
    public void increaseScore(float points)
    {
        score += points;
    }


    //Pause the game when the progress bar has fully filled
    private void checkForGameOver()
    {
        if (progress >= 1)
        {
            gameOver = true;
            var objects = FindObjectsOfType(typeof(GameObject));
            foreach (GameObject go in objects)
                go.SendMessage("OnPauseGame", SendMessageOptions.DontRequireReceiver);
        }
    }

    private void OnPauseGame()
    {
        paused = true;
    }

    private void OnResumeGame()
    {
        paused = false;
    }


    /*----------------------------------------------------
     * updates the spawn rates of mice and feathers to
     * increase # of mice spawned and decrease
     * # of feathers spawned
     * */
    private void changeSpawns()
    {
        if (MouseSpawn == null)
            MouseSpawn = GameObject.FindGameObjectWithTag(MouseSpawnTag);
        if (featherSpawn == null)
            featherSpawn = GameObject.FindGameObjectWithTag(featherTag);
        var currentTime = Time.time;
        if (currentTime - startTime > spawnRateChange)
        {
            MouseSpawn.GetComponent<MiceSpawn>().increaseSpawn(totalMiceToAdd);
            featherSpawn.GetComponent<FeatherSpawn>().decreaseSpawn(totalFeathersToRemove);
            startTime = currentTime;
        }
    }
}