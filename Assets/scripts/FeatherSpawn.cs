﻿using UnityEngine;

public class FeatherSpawn : MonoBehaviour
{
    public GameObject featherObject;
    private float numOfFeathers;
    private bool paused;
    public float totalNumOfFeathers = 3f;

    // Use this for initialization
    private void Start()
    {
    }

    /*----------------------------------------------
     * randomly spawns feathers throughout the map
     */
    private void spawnFeather()
    {
        if (numOfFeathers <= totalNumOfFeathers)
        {
            var position = new Vector3(Random.Range(-45f, 45f), 3f, 0f);
            Instantiate(featherObject, position, Quaternion.identity);
            numOfFeathers++;
        }
    }

    public void removeFeather()
    {
        numOfFeathers--;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!paused)
            spawnFeather();
    }

    private void OnPauseGame()
    {
        paused = true;
    }

    private void OnResumeGame()
    {
        paused = false;
    }

    public void decreaseSpawn(float decreaseFeathers)
    {
        if (totalNumOfFeathers >= 0)
            totalNumOfFeathers -= decreaseFeathers;
    }
}